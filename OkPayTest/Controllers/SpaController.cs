﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OkPayTest.Controllers
{
    [Authorize]
    public class SpaController : Controller
    {
        // GET: Spa
        public ActionResult Index()
        {
            ViewBag.From = (string)this.RouteData.Values["from"];
            ViewBag.To = (string)this.RouteData.Values["to"];
            return View();
        }
    }
}