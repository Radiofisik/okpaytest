﻿(function () {
    'use strict';

    angular.module('okpayapp.restclient', [])
        .service('RestClient', ['$q', '$http', function ($q, $http) {
            var model = this,
                URLS = {
                    currencyList: '/api/currencylist',
                    changerList: '/api/changerlist/getbycurrency'
                };

            model.getChangersByCurrencies = function (from, to) {
                var deferred = $q.defer();
                var params = {};
                params.from = from;
                params.to = to;

                var request = {
                    method: 'GET',
                    url: URLS.changerList,
                    params: params
                };

                $http(request).then(function (response) {
                    deferred.resolve(response);
                });

                return deferred.promise;
            };

            model.getCurrencies = function () {
                var deferred = $q.defer();

                var request = {
                    method: 'GET',
                    url: URLS.currencyList
                };

                $http(request).then(function (response) {
                    deferred.resolve(response);
                });

                return deferred.promise;
            };

        }]);

})();