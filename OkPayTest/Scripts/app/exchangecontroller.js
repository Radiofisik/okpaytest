﻿(function () {
    'use strict';

    var app = angular.module('okpayapp.exchangecontroller', ['okpayapp.restclient']);

    app.controller('ExchangeController', ['$scope', 'RestClient',
    function ($scope, RestClient) {

        RestClient.getCurrencies().then(function (res) {
             $scope.currencyList=res.data;
        });

        $scope.fromClick = function (currency) {
            $scope.from = currency.UrlFriendlyName;
            checkIfAllSelected();
        };

        $scope.toClick = function (currency) {
            $scope.to = currency.UrlFriendlyName;
            checkIfAllSelected();
        };

        $scope.init = function () {
            checkIfAllSelected();
        };

        function checkIfAllSelected() {
            if ($scope.from && $scope.to) {
                var url = $scope.from + "_TO_" + $scope.to;
                window.history.replaceState({}, null, url);
                loadChangers();
            }
        }

        function loadChangers() {
            RestClient.getChangersByCurrencies($scope.from, $scope.to).then(function (res) {
                $scope.changerList = res.data;
            });
        }
      
    }]);
    
})();
