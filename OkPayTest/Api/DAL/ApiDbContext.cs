﻿using OkPayTest.Api.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.ConstrainedExecution;
using System.Web;

namespace OkPayTest.Api.DAL
{
    public class ApiDbContext: DbContext
    {
        public ApiDbContext() : base("DefaultConnection"){}
        public DbSet<Currency> CurrencyDbSet { get; set; }
        public DbSet<Changer> ChangerDbSet { get; set; }
    }
}