﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OkPayTest.Api.Entities
{
    public class Changer
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Currency> CurrencyList { get; set; } = new List<Currency>();
    }
}