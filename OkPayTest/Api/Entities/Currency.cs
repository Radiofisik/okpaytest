﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OkPayTest.Api.Entities
{
    public class Currency
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        //[Unique] should be made unique, this is supported by fluent api in EF core, not EF6
        [Required]
        [MaxLength(100)]
        public string UrlFriendlyName { get; set; }
        public List<Changer> ChangerList { get; set; } = new List<Changer>();
    }
}