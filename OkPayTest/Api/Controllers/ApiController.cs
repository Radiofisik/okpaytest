﻿using OkPayTest.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace OkPayTest.Controllers.Api
{
    public class OkPayApiController :  ApiController
    {
        // GET api/OkPayApi
        public CurrencyDto Get()
        {
            return new CurrencyDto() { Name = "test" };
        }

    }
}
