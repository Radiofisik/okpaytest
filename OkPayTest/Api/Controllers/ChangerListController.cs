﻿using AutoMapper;
using OkPayTest.Api.DAL;
using OkPayTest.Api.DTO;
using OkPayTest.Api.Entities;
using OkPayTest.DTO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace OkPayTest.Api.Controllers
{
    public class ChangerByCurrencyRequest
    {
        public string From { get; set; }
        public string To { get; set; }
    }
    [Authorize]
    public class ChangerListController: ApiController
    {
        private ApiDbContext context;
        public ChangerListController()
        {
            context=new ApiDbContext();
        }

        public IEnumerable<ChangerDto> Get()
        {
            //for the sake of simplicity don't use pagination (take and skip) here
            var currencyList = context.ChangerDbSet.ToList();
            var dtos = Mapper.Map<IEnumerable<ChangerDto>>(currencyList);
            return dtos;
        }

        [HttpGet]
        [Route("api/changerlist/getbycurrency")]
        public IEnumerable<ChangerDto> GetByCurrency([FromUri] ChangerByCurrencyRequest request)
        {
            var query = @"SELECT ch.Id as ""Id"", ch.Name as ""Name"", ch.Description as ""Description""  FROM dbo.CurrencyChangers cc1
                            JOIN dbo.CurrencyChangers cc2 ON cc2.Changer_Id = cc1.Changer_Id
                            JOIN dbo.Currencies c1 ON cc1.Currency_Id = c1.Id
                            JOIN dbo.Currencies c2 ON cc2.Currency_Id = c2.Id
                            JOIN dbo.Changers ch ON cc1.Changer_Id = ch.Id
                            WHERE c1.UrlFriendlyName = @from
                            AND c2.UrlFriendlyName = @to";

            var currencyList = context.ChangerDbSet.SqlQuery(query, new SqlParameter("@from", request.From), new SqlParameter("@to", request.To)).ToList();
            var dtos = Mapper.Map<IEnumerable<ChangerDto>>(currencyList);
            return dtos;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                    context = null;
                }
            }
        }

    }
}