﻿using AutoMapper;
using OkPayTest.Api.DAL;
using OkPayTest.Api.Entities;
using OkPayTest.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace OkPayTest.Api.Controllers
{
    [Authorize]
    public class CurrencyListController: ApiController
    {
        private ApiDbContext context;
        public CurrencyListController()
        {
            context=new ApiDbContext();
        }

        public IEnumerable<CurrencyDto> Get()
        {
            //for the sake of simplicity don't use pagination (take and skip) here
            var currencyList = context.CurrencyDbSet.ToList();
            var dtos = Mapper.Map<IEnumerable<CurrencyDto>>(currencyList);
            return dtos;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                    context = null;
                }
            }
        }
    }
}