﻿using AutoMapper;
using OkPayTest.Api.DTO;
using OkPayTest.Api.Entities;
using OkPayTest.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkPayTest.Api.Helpers
{
    public class AutomapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Currency, CurrencyDto>();
                cfg.CreateMap<Changer, ChangerDto>();
            });
        }
    }
}