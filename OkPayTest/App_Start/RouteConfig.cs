﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace OkPayTest
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "Spa",
               url: "{from}_TO_{to}",
               defaults: new { controller = "Spa", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "SpaDirect",
               url: "spa/{from}_TO_{to}",
               defaults: new { controller = "Spa", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Spa", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
